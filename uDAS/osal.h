/****************************************************************************
 *
 *   Copyright (c) 2019-2020 jiezhi320. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name AMOV nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED TeamWARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
/**
 * @file oasl.h
 * Created on: 2019-10-10 
 */
 
#ifndef _OSAL_H_
#define _OSAL_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "rtthread.h"

#define OS_IPC_WAITING_FOREVER      RT_WAITING_FOREVER

#define OS_SEM_HANDLER		          rt_sem_t	
#define OS_SEM_CREATE(name, value)  rt_sem_create(name, value, RT_IPC_FLAG_PRIO)	
#define OS_SEM_SEND(x)		          rt_sem_release(x)
#define OS_SEM_PEND(x, timeout)	    rt_sem_take(x, timeout)

#define OS_MALLOC(size)			        rt_malloc(size)
#define OS_FREE(ptr)				        rt_free(ptr)

#define OS_ENTER_CRITICAL			      rt_enter_critical()
#define OS_EXIT_CRITICAL			      rt_exit_critical()


#ifdef __cplusplus
}
#endif

#endif /* _OSAL_H_ */


