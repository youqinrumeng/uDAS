/****************************************************************************
 *
 *   Copyright (c) 2019-2020 jiezhi320. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name AMOV nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED TeamWARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
/**
 * Data Advertise and Subscribe
 * @file uDAS.h
 * Created on: 2019-10-10 
 */
 
#ifndef _UDAS_H_
#define _UDAS_H_

#include <ctype.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "osal.h" 

#ifdef __cplusplus
extern "C" {
#endif


typedef struct das_node *das_node_t;

struct das_node{
    
    volatile bool updated; /* for poll mode*/
	  OS_SEM_HANDLER sem;    /* for pend mode*/
	  das_node_t next;
};


typedef struct{

	const char *o_name;        /* unique object name */
	const uint32_t o_size;     /* object size */
	void *o_metadata;          /* object data pointer */
	
	das_node_t item_head;      /* object linklist head */

}das_hub;



/**
 * Generates a pointer to the uDAS metadata structure for
 * a given topic.
 *
 * The topic must have been declared previously in scope
 * with DAS_DECLARE().
 *
 * @param _name		The name of the topic.
 */
#define DAS_ID(_name)		&__das_##_name

/**
 * Declare (prototype) the uDAS metadata for a topic.
 *
 * @param _name		The name of the topic.
 */
#define DAS_DECLARE(_name)		extern /*const*/ das_hub __das_##_name;


/**
 * Define (instantiate) the uDAS metadata for a topic.
 *
 * The uDAS metadata is used to help ensure that updates and
 * copies are accessing the right data.
 *
 * Note that there must be no more than one instance of this macro
 * for each topic.
 */
#define DAS_DEFINE(_name, _object_struct)		\
	/*const */das_hub __das_##_name = {	\
		#_name,					 \
		sizeof(_object_struct), \
		NULL,			       \
		NULL};
	
		
int32_t das_advertise(das_hub* hub);
int32_t das_publish(das_hub* hub, const void* data);		
das_node_t das_subscribe(das_hub* hub);	
int32_t das_update_poll(const das_hub* hub, das_node_t pnode, void* buffer);
int32_t das_update_pend(const das_hub* hub, das_node_t pnode, void* buffer, int32_t timeout);


#ifdef __cplusplus
}
#endif

#endif /* _UDAS_H_ */
		
		



