/****************************************************************************
 *
 *   Copyright (c) 2019-2020 jiezhi320. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name AMOV nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED TeamWARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
/**
 * @file uDAS_test.c
 * Created on: 2019-10-10 
 */

#include "uDAS.h"

typedef struct{
	 int16_t x_raw;
 	 int16_t y_raw;
	 int16_t z_raw; 
}imu_obj;

imu_obj mpu6000_pub, mpu6000_sub1, mpu6000_sub2;//advtise object




DAS_DEFINE(IMU, mpu6000_pub)

das_node_t imu_sub1, imu_sub2;



void publish_entry(void *p)
{
    das_advertise(DAS_ID(IMU));
	
	  while(1)
		{
			mpu6000_pub.x_raw++;
			mpu6000_pub.y_raw++;
			mpu6000_pub.z_raw++;

		    das_publish(DAS_ID(IMU), &mpu6000_pub);
			
			rt_thread_delay(500);
			  
		}	
	
}	

void update_entry(void *p)
{
    imu_sub1 = das_subscribe(DAS_ID(IMU));
	imu_sub2 = das_subscribe(DAS_ID(IMU));

    while(1)
		{
		    das_update_pend(DAS_ID(IMU), imu_sub1, &mpu6000_sub1, RT_WAITING_FOREVER);
			das_update_pend(DAS_ID(IMU), imu_sub2, &mpu6000_sub2, RT_WAITING_FOREVER);

			// rt_thread_delay(200);		
		}	
	
}	

void das_test(void)
{
    rt_thread_t thread_pub, thread_sub;
	
	  thread_pub = rt_thread_create("publish",
	                                 publish_entry,
	                                 0,
	                                 512,
	                                 17,
	                                 20
	                               );                
    if (thread_pub)
		    rt_thread_startup(thread_pub);
		
	  thread_sub = rt_thread_create("sub",
	                                 update_entry,
	                                 0,
	                                 512,
	                                 17,
	                                 20
	                               );                
    if (thread_sub)
		    rt_thread_startup(thread_sub);		
}	
