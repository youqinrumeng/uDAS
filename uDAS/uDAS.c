/****************************************************************************
 *
 *   Copyright (c) 2019-2020 jiezhi320. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name AMOV nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED TeamWARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
/**
 * Data Advertise and Subscribe 
 * @file uDAS.c
 * Created on: 2019-10-10 
 */
#include "uDAS.h"
#include "utlist.h" /* for LinkList */


static void node_append(das_node_t *head, das_node_t node);

/*
 function name ��das_advertise
                 topic advertise
 result 
 > 0 : ok
 ==0 : hub has already advertised
 < 0 : malloc failed
*/
int32_t das_advertise(das_hub* hub)
{
    int32_t res = 1;
	
	  if(hub->o_metadata != NULL){
	  	 return 0;
	  }
	
	  OS_ENTER_CRITICAL;
	
	  hub->o_metadata = OS_MALLOC(hub->o_size);
	  if(hub->o_metadata == NULL){
        res = -1;
	  }
	  else{
			  hub->item_head = NULL;
	  }
	
	  OS_EXIT_CRITICAL;
	
	  return res;	
}

/*
function name: das_publish
               publish a topic

 result 
 > 0 : ok
 ==0 : hub is not advertised 
 < 0 : null ptr
*/
int32_t das_publish(das_hub* hub, const void* data)
{
    int32_t res = 1;
	  das_node_t pnode = NULL;		
	
	  if(data == NULL){
		    return -1;
	  }
	
	  if(hub->o_metadata == NULL){
		    return 0;
	  }
	
	  OS_ENTER_CRITICAL;
	  memcpy(hub->o_metadata, data, hub->o_size);
		
		LL_FOREACH(hub->item_head, pnode){
		    pnode->updated = true;
		    OS_SEM_SEND(pnode->sem);			
		}
		
	  OS_EXIT_CRITICAL;
	
	return res;
}	

/*
function name: das_subscribe
          subscribe a topic

 result 
  > 0 :  ok
 NULL : malloc failed
*/
das_node_t das_subscribe(das_hub* hub)
{
	  //alloc memery for new node
	  das_node_t pnode = (das_node_t)OS_MALLOC(sizeof(struct das_node));
	
	  if(pnode == NULL){
	      return NULL;
	  }
		
		pnode->updated = false;
	  pnode->next = NULL;		
	  pnode->sem = OS_SEM_CREATE(hub->o_name, 0);
	  if(pnode == NULL){
	      return NULL;
	  }		
	
	  OS_ENTER_CRITICAL;
		/*insert item to linklist*/
    node_append(&(hub->item_head), pnode);
		
	  OS_EXIT_CRITICAL;
	
	  return pnode;	
}

/*
function name: das_check
               das_check a topic is update or not

 result 
 ture  :  node has updated
 false :  node has not updated
*/
bool das_check(const das_node_t pnode)
{
    if (pnode == NULL){
		    return false;
		}
		else{
		    return pnode->updated;
		}
}


/*
function name: das_update_poll
               get object from hub to buffer

 result 
 > 0 : ok
 ==0 : hub is not advertised 
 < 0 : null ptr
*/
int32_t das_update_poll(const das_hub* hub, das_node_t pnode, void* buffer)
{
    int32_t res = 1;
	
	  if((buffer == NULL) || (pnode == NULL)) {
		    return -1;
	  }
	
	  if(hub->o_metadata == NULL){
		    return 0;
	  }
		
	  OS_ENTER_CRITICAL;
		if (pnode->updated){
	      memcpy(buffer, hub->o_metadata, hub->o_size);
	      pnode->updated = false;
			  /* TODO reset sem ?*/
		}	
		else{
		    res = -2;
		}
	  OS_EXIT_CRITICAL;
	
	  return res;	
}	


/*
function name: das_update_pend
               get object from hub to buffer

 result 
 > 0 : ok
 ==0 : hub is not advertised 
 < 0 : null ptr
*/
int32_t das_update_pend(const das_hub* hub, das_node_t pnode, void* buffer, int32_t timeout)
{
    int32_t res = 1;
	
	  if((buffer == NULL) || (pnode == NULL)) {
		    return -1;
	  }
	
	  if(hub->o_metadata == NULL){
		    return 0;
	  }
		
	  OS_ENTER_CRITICAL;
		if (OS_SEM_PEND(pnode->sem, timeout) == 0){
		    pnode->updated = false;
			  memcpy(buffer, hub->o_metadata, hub->o_size);
		}	
		else{
		    res = -2;
		}
	  OS_EXIT_CRITICAL;
	
	  return res;	
}	

/*
  LDECLTYPE not avilad in mdk complier
*/
static void node_append(das_node_t *head, das_node_t node)
{
    do {                                                                                           
      das_node_t _tmp;  
			
      node->next = NULL;                                                                            
      if (*head) {                                                                                  
        _tmp = *head;                                                                               
        while (_tmp->next) { _tmp = _tmp->next; }                                                  
        _tmp->next = node;                                                                          
      }
			else {                                                                                     
        *head = node;                                                                              
      }
	  }			
    while (0);	
}	



